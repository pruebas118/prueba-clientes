import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent {
  constructor(private router: Router) {}

  consultar(): void {
    this.router.navigate(['/entrada-cliente']);
  }

  informacion(): void {
    this.router.navigate(['/resumen-cliente']);
  }

}
