import { Component, OnInit } from '@angular/core';
import { Cliente } from '../models/cliente';
import { ServiciosService } from '../service/servicios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resumen-cliente',
  templateUrl: './resumen-cliente.component.html',
  styleUrls: ['./resumen-cliente.component.css']
})
export class ResumenClienteComponent implements OnInit{
  cliente!: Cliente;
  
  constructor(private _servicio: ServiciosService, private router: Router) {}

  ngOnInit(): void {
    this.cliente = this._servicio.getCliente();
    console.log(this.cliente);
  }
  
  volver(): void {
    this.router.navigate(['/entrada-cliente']);
  }
}
