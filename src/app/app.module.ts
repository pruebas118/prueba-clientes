import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiciosService} from './service/servicios.service';
import { EntradaClienteComponent } from './entrada-cliente/entrada-cliente.component';
import { ResumenClienteComponent } from './resumen-cliente/resumen-cliente.component';
import { ThousandsSeparatorDirective } from './thousands-separator.directive';
import { PrincipalComponent } from './principal/principal.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    EntradaClienteComponent,
    ResumenClienteComponent,
    ThousandsSeparatorDirective    
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [ServiciosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
