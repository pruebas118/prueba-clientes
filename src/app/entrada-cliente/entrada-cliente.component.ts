import { Component } from '@angular/core';
import { ServiciosService } from '../service/servicios.service';
import { Router } from '@angular/router';
import { Cliente } from '../models/cliente';


@Component({
  selector: 'app-entrada-cliente',
  templateUrl: './entrada-cliente.component.html',
  styleUrls: ['./entrada-cliente.component.css']
})
export class EntradaClienteComponent {
  cliente?: Cliente;
  selectedTipoDocumento = '';
  numeroDocumento = '';
  buscarHabilitado = false;

  constructor(
    private _servicio: ServiciosService, private router: Router
  ) { }

  habilitarBuscar() {
    const numeroDocumentoControl = this.numeroDocumento;
    let numeroSinPuntos = numeroDocumentoControl.replace(/\./g, '');
    const numeroDocumentoIsValid = /^[0-9]*$/.test(numeroSinPuntos);
    const numeroDocumentoLengthValid = numeroSinPuntos.length >= 8 && numeroSinPuntos.length <= 11;
  
    this.buscarHabilitado = this.selectedTipoDocumento !== '' && numeroDocumentoIsValid && numeroDocumentoLengthValid;
  }
  

  buscarCliente() {
    this.numeroDocumento = this.numeroDocumento.replace(/\./g, '');
    this._servicio.buscarClientesPorDocumento(this.selectedTipoDocumento, this.numeroDocumento).subscribe(
      (response: any) => {
        console.log(response);
        this._servicio.setCliente(response[0]);
        this.router.navigate(['/resumen-cliente']);
      },
      error => {
        alert(error.error.mensaje);
        console.log(error);
      }
    );
    
  }

  volver(): void {
    this.router.navigate(['/']);
  }

}
