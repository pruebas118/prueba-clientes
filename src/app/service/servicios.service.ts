import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cliente } from '../models/cliente';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*', 'Authorization':'authkey'})
};

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  private cliente!: Cliente;

  private url: string = "http://localhost:8090/prueba/cliente/";

  constructor(  private http:HttpClient ) { }

  obtenerTodosLosClientes() {
    return this.http.get(this.url, httpOptions);
  }

  buscarClientesPorDocumento(tipoDocumento: string, numeroDocumento: string): Observable<any> {
    const urlBuscar = `${this.url}buscar?tipoDocumento=${tipoDocumento}&numeroDocumento=${numeroDocumento}`;
    return this.http.get(urlBuscar, httpOptions);
  }

  obtenerClientePorId(id: number) {
    return this.http.get(this.url + id, httpOptions);
  }

  crearCliente(cliente: Cliente) {
    return this.http.post(this.url, cliente, httpOptions);
  }

  actualizarCliente(cliente: Cliente, id: number) {
    return this.http.put(this.url + id, cliente, httpOptions)
  }

  eliminarCliente(id: number) {
    return this.http.delete(this.url + id, httpOptions);
  }

  setCliente(cliente: Cliente) {
    this.cliente = cliente;
  }

  getCliente(): Cliente {
    return this.cliente;
  }
}
