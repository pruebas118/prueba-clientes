import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appThousandsSeparator]'
})
export class ThousandsSeparatorDirective {

  constructor(private el: ElementRef) { }

  @HostListener('input', ['$event.target.value'])
  onInput(value: string): void {
    value = value.replace(/\D/g, '');
    const formattedValue = new Intl.NumberFormat("de-DE").format(parseInt(value));
    this.el.nativeElement.value = formattedValue;
  }
}
