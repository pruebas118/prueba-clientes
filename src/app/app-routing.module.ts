import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrincipalComponent } from './principal/principal.component';
import { EntradaClienteComponent } from './entrada-cliente/entrada-cliente.component';
import { ResumenClienteComponent } from './resumen-cliente/resumen-cliente.component';

const routes: Routes = [
  { path: '', component: PrincipalComponent},
  { path: 'principal', component: PrincipalComponent},
  { path: 'entrada-cliente', component: EntradaClienteComponent },
  { path: 'resumen-cliente', component: ResumenClienteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
