import { TestBed } from '@angular/core/testing';
import { ElementRef } from '@angular/core';
import { ThousandsSeparatorDirective } from './thousands-separator.directive';

describe('ThousandsSeparatorDirective', () => {
  it('should create an instance', () => {
    const el = {
      nativeElement: document.createElement('input') // Simulación de un elemento input
    } as ElementRef;

    const directive = new ThousandsSeparatorDirective(el);
    expect(directive).toBeTruthy();
  });
});
